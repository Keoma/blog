---
title: Projects
layout: page
order: 6
permalink: /projects/
---

SolSystem
=========

[SolSystem](solsystem.io)
<iframe width="560" height="315" src="https://www.youtube.com/embed/ruMeYcZB7t8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### SaveThePeaches:

![SaveThePeaches](/assets/imgs/savethepeaches.png)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/_qGSH8l0Vkk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### SmartMarina:

[SmartMarina](smartmarina.org)

* [www.inria.fr/centre/paris/actualites/l-equipe-eva-reinvente-le-port-connecte](https://www.inria.fr/centre/paris/actualites/l-equipe-eva-reinvente-le-port-connecte)
* [www.herault-tribune.com/articles/44286/le-cap-detrsquo%3Bagde-etnbsp%3B-le-port-en-passe-detrsquo%3Betre-connecteetnbsp%3B/](http://www.herault-tribune.com/articles/44286/le-cap-detrsquo%3Bagde-etnbsp%3B-le-port-en-passe-detrsquo%3Betre-connecteetnbsp%3B/)
* [atoutnautic.fr/blog/article/10230](http://atoutnautic.fr/blog/article/10230)
* [www.youtube.com/watch?v=LUcLE8D0RbM0](https://www.youtube.com/watch?v=LUcLE8D0RbM0)

### SnowHow:

[SnowHow.io](snowhot.io)

* [https://fbf.berkeley.edu/grantee-spotlight-smart-sensor-technology](https://fbf.berkeley.edu/grantee-spotlight-smart-sensor-technology)

<iframe width="560" height="315" src="https://www.youtube.com/embed/d0oE1xtViZs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

