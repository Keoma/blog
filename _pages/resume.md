---
title: Resume
layout: page
order: 2
permalink: /resume/
---

Talks
------

* YunoHost and the Internet Cubes [video](https://media.freifunk.net/v/yunohost-and-the-internet-cube-brique-internet), [BattleMesh V12](https://www.battlemesh.org/BattleMeshV12), Paris, July 2019.

Teaching
--------

 * Course on Linux Network Tools and C Programming (sockets) at <a href="http://www.ensta-paristech.fr/en" rel="noopener">ENSTA ParisTech</a>, 2019
 * Course on IoT with [Thomas Watteyne](//twatteyne.wordpress.com), <a href="http://u2is.ensta-paristech.fr/members/geller/index.php?lang=en" rel="noopener">Benoit Geller</a> and <a href="https://www.researchgate.net/profile/Dominique_Barthel" rel="noopener">Dominique Barthel</a> at <a href="http://www.ensta-paristech.fr/en" rel="noopener">ENSTA ParisTech</a>, 2018.
 * Course on Linux Network Tools and C Programming (sockets) at <a href="http://www.ensta-paristech.fr/en" rel="noopener">ENSTA ParisTech</a>, 2018
 * 3-hours hands-on tutorial on <a href="http://www.linear.com/products/smartmesh_ip">SmartMeshIP</a>, Node-Red and IBM Bluemix. <a href="http://www.ucl.ac.uk/">University College London</a>. Together with <a href="https://www.ee.ucl.ac.uk/staff/academic/uceergr">Ryan Grammenos</a>, 1rst March 2017.
 * Course on IoT with [Thomas Watteyne](//twatteyne.wordpress.com), <a href="http://u2is.ensta-paristech.fr/members/geller/index.php?lang=en" rel="noopener">Benoit Geller</a> and <a href="https://www.researchgate.net/profile/Dominique_Barthel" rel="noopener">Dominique Barthel</a> at <a href="http://www.ensta-paristech.fr/en" rel="noopener">ENSTA ParisTech</a>, 2017.
 * Intensive 1-week course on IoT, with associated hands-on labs. <a href="https://www.ensta-paristech.fr/">ENSTA ParisTech</a>. Together with <a href="https://twatteyne.wordpress.com">Thomas Watteyne</a> and Dominique Barthel,  12-15th December 2016.

Honors and Awards
-------------------

 * Runner up ***<a href="http://secon2016.ieee-secon.org/">IEEE SECON 2016</a>*** Best Demo Award with “A Demo of the PEACH IoT-based Frost Event Prediction System for Precision Agriculture”, Keoma Brun-Laguna, Ana Laura Diedrichs, Javier Emilio Chaar, Diego Dujovne, Juan Carlos Taffernaberry, Gustavo Mercado and Thomas Watteyne, 28 June 2016.

Attending
----------

 * Workshop «Agriculture Numérique en Afrique», organized by [***LIRIMA***](//lirima.inria.fr), 3-4th April 2018, Montpellier, France.
 * Journées non thématiques [***RESCOM 2017***](//team.inria.fr/coati/seminars-and-conferences/journees-non-thematiques-rescom/), 12th January, Sophia-Antipolis, France.
 * ACM International Conference on Mobile Computing and Networking ([***MobiCom***](//www.sigmobile.org/mobicom/2016/)), Workshop on Challenged Networks ([***CHANTS'16***](//www.acm-chants.org/)), 7th October 2016, NYC, New York.
 * 27st Annual IEEE International Symposium on Personal, Indoor and Mobile Radio Communications ([***PIMRC***](//www.ieee-pimrc.org/2016/)), 4-7th September 2016, Valencia, Spain.
 * [IEEE SECON 2016](//secon2016.ieee-secon.org/) Demo session, 28 June 2016, London, UK.
 * Journées non thématiques [***RESCOM 2016***](//team.inria.fr/fun/journees-non-thematiques-rescom/), 12th January, Lille, France.

Visiting:
---------

 * [Ryan Grammenos](//www.linkedin.com/in/rcgrammenos), 1 day, February 2018, UCL, London.
 * Deployment of sensors for a course on WSN at UCL
 * <a href="http://glaser.berkeley.edu/glaserdrupal/">Steven D. Glaser</a>, 3 weeks, Summer 2016, Berkeley, California.
 * Deployment of sensors in the Sierra Nevada for the project <a href="http://www.snowhow.io" rel="noopener">SnowHow</a>
 * <a href="http://www.frm.utn.edu.ar/posgrado/?portfolio=ing-gustavo-mercado">Gustavo Mercado</a>, 3 weeks, August 2015, Mendoza, Argentina.
 * Deployment of sensors in a peach orchards for the project <a href="http://www.savethepeaches.com">SaveThePeaches
   * <a href="https://www.researchgate.net/profile/Diego_Dujovne">Diego Dujovne</a>, 1 week, September 2015, Santiago, Chile.
   * Starting project on sensor actuation with LegoMindstorms

Volunteering:
-------------

 * 2nd EAI International Conference on Interoperability in IoT (<a href="http://interoperabilityiot.org/2016/show/home" rel="noopener"><strong>InterIoT2016</strong></a>), October 26-27 2016, Paris, France.
