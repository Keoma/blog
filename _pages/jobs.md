---
title: Jobs
layout: page
order: 3
permalink: /jobs/
---

Freelance
----

[Lifehive](//www.lifehive.io)

October-November 2023 - 2 months

Lifehive develops digital solution to help beekeepers eliminating Varroa mites without using pesticides (using heating).
I worked as freelance for them, and initiated their IoT architecture (data collection, storing and monitoring).

---

CTO at Falco
----

[![Wattson Elements - Falco]({{ "assets/imgs/falco.png"  | absolute_url }})](//wefalco.com)

Set sail for the smart marina!

January 2019 to June 2023 - 4.5 years

I co-founded the Falco company after my thesis, together with Elsa Nicole, Olivier Laporte and Thomas Watteyne.
Together, we built a solution to help marina managers develop their customer services in a secure and reliable way.
I recruited and managed a team of up to 6 people, and we built a set of software and hardware tools that are still in use today in multiple marinas internationally.

---

PhD Student
------------

[![inria]({{ "assets/imgs/inria.png" | absolute_url }})](//inria.fr)

2016-2019 – 3 years
with [#Thomas Watteyne](//twatteyne.wordpress.com/) and [#Pascale Minet](//www.researchgate.net/profile/Pascale_Minet)

Subject : Deterministic Networking for the Industrial Internet of Things

 * Deployment and implementation of Real-World Wireless Sensor Networks
 * Study of the TSCH protocol limits and trade-offs

---

Intern
-------

[![linear]({{ "assets/imgs/linear.jpg" | absolute_url }})](//www.linear.com/dust_networks/)

2017 – 4 months
with [#Dave Bacher](//www.linkedin.com/in/davidbacher)

 * Packet dissection and analysis (Wireshark)

---

Research assistant
-------------------

[![icube]({{ "assets/imgs/icube.png" | absolute_url}})](//icube-reseaux.unistra.fr/fr/index.php/Accueil)

2015 – 6 months
with [#Fabrice Theoleyre](//www.theoleyre.eu/)

Subject : Load balancing in 6TiSCH

 * Related works analysis
 * Solution proposal and implementation in OpenWSN
 * Experimental analysis with IoTLab
 * Research paper writing

---

Research assistant
-------------------

[![umpc]({{ "assets/imgs/upmc.png" | absolute_url }})](//upmc.fr)

2014 – 3 months
with [#Marcelo Dias de Amorim](//www.linkedin.com/in/mdiasdeamorim)

Subject : Device-to-Device energy consumption analysis

 * Related works analysis
 * Testbed setup (Android, Adafruit INA219)
 * Measures and interpretation
 * Research paper writing

---

Developer
----------

![alaloop]({{ "assets/imgs/alaloop.png" | absolute_url }})

2012 – 3 months

Automation of installation and settings for remote agents:

 * Tool: InnoSetup
 * Language: Pascal Script
 * Windows settings: register, network, drivers and modules


