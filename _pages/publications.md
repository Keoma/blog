---
title: Publications
layout: page
order: 6
permalink: /publications/
---

[Thesis]({{ "/assets/pdfs/2018-12-18_brun18deterministic.pdf" | absolute_url }})
[Thesis-Slides]({{ "/assets/pdfs/2018-12-18_brun18deterministic_slides.pdf" | absolute_url }})
[HAL](//hal.archives-ouvertes.fr/search/index/?qa[authIdHal_s][]=keoma-brun-laguna)
[Bibtex]({{ "/assets/publications.bib" | absolute_url }})
[Scholar](https://scholar.google.fr/citations?user=cfseY5QAAAAJ)

{% bibliography %}

