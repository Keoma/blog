---
title: Hobbies
layout: page
order: 7
permalink: /hobbies/
---

Computing
---------

 * Co-founder of the [Cloud Girofle](//girofle.cloud).
 * Volunteer at [franciliens.net](//franciliens.net). In charge of the grouped command of the [internetcube](//internetcu.be) project
 * Volunteer at the [Amigos de Jesus fondation](//www.amigosdejesus.cl) in Chile
 * Volunteer in a health care center: Development of a management system using Virtual Basic - Dundee, Scotland (2012)
 * IT Teacher for beginners - Anglet, France (2010-2011)
 * Member of the winning team of the 24 hours of innovation of the [ESTIA](//www.estia.fr) - Bidart, France (2011)
 * President of a young association: Creation of a society based on the computing field and collection of more than 800€ to create an IT pole - Parempuyre, France (2006)

![Computing]({{ "assets/imgs/computing.png" | relative_url }})

---

Art
----------

 * I practice percussion since the age of 8
 * I created a percussion society in Abertay University - Dundee, Scotland (2012)
 * I practice Capoeira


![Percussion]({{ "assets/imgs/percussion.png" | relative_url }})
![Capoeira]({{ "assets/imgs/capoeira.png" | relative_url }})

---

Climbing
--------

 * I like to climb on rocks

![Climbing]({{ "assets/imgs/climbing.png" | relative_url }})
