---
title: Studies
layout: page
order: 5
permalink: /studies/
---

Master (2nd Year)
=================

![UPMC](/assets/imgs/upmc.png)

Université Pierre et Marie Curie (UPMC)
UFR Info-p6
Master 2 : Computing Networks

1st semester:
-------------

 * Research methodology: reading, writing, reviewing, presenting
 * Ubiquitous networks: sensor network, BANet, robot networks, Byzantine networks
 * Networks measurement: static/dynamic graphs, Internet metrology
 * Smart Mobility: VANet, project on mobile pollution measurement
 * System evaluation and performance: Markov chain, queuing models
 * English: TOEIC

2nd semester:
-------------

 * Internship with Fabrice Theoleyre at ICube (Strasbourg).
 * Evaluation & performance in 6TiSCH networks.
 * Load-balancing and energy repartition using 6TiSCH Tracks.

---

Master (1rst Year)
==================

![Unistra](/assets/imgs/unistra.png)

Université de Strasbourg (UNISTRA)
UFR Math-Info
Master 1: Computing Networks and Embedded Systems

1st semester:
-------------

 * Compilation (lexical and syntactical analysis : Lex et Yacc)
 * Embedded systems and electronic
 * Network security
 * Protocols and mechanisms (network, transport and application layers)
 * Performance analysis (simulation models and processes)

2nd semester:
-------------

 * Network administration (Cisco, SNMP, MIB, DHCP, DNS)
 * Advanced compilation (LLVM, parallelism)
 * Real time system and computing (Scheduling policies, POSIX4, RTAI)
 * Carrier networks (BGP, MPLS, VPN, telecommunication)
 * Business networks (Business Objects, Microsoft Networks, networks legal aspect)

---

BSc(Hons) Computing Networks
============================

![Abertay](/assets/imgs/abertay.png)

University of Abertay University - Dundee
Sending institution : UFR sciences et techniques de la côte Basque

1st semester:
-------------

 * Database and Internet Application Design : database design (EERM), normalisation, SQL, DDL, strategic planning
 * Group Project : project principle, management, quality assurance
 * Networking and Security: network models, protocols, security & authentication
 * Mobile Programming : Mobile devices, cycle development, high and low UI, storage, commercial deployment

2nd semester:
-------------

 * Networking : Linux file system, system administration, network configuration, scripting
 * Network Security : legal issues, threats, mitigation, methods of attacking systems
 * Group Project: design, implementation, testing and evaluation, communication
 * Server side development : Application architecture, server side scripting, databases, optimisation

---

Diplôme Universitaire Technologique (DUT)
=========================================

![UNISTRA](/assets/imgs/unistra.png)

Université de Pau et des Pays de l'Adour
IUT Bayonne

1st year:
---------

 * Algorithmic and Programming
 * Architecture, Systems and Networks
 * Software engineering
 * Mathematics
 * Economy and Management
 * English
 * Communication

2nd year:
---------

 * Programming reuse
 * Embedded technology
 * UML/Java
 * Mathematics
 * Economy and Management
 * English
 * Communication
 * Project : Timelines
 * Internship: Alaloop
