Together with friends we decided to join efforts and administrate our own online services. We had some issue when setting up virtualization on Debian Stretch and I wanted to share what we learned.

We chose to use what we believe to be the "most standard" solution: KVM with QEMU, and libvirt. I expected it to work out of the box and I was wrong. Following the <a href="https://wiki.debian.org/KVM">Debian Wiki</a> was instructive but not sufficient.

We want to install a VM running Debian Jessie on a host running Debian Stretch.
<h2>Installation</h2>
<pre>sudo apt install qemu-kvm libvirt-clients libvirt-daemon-system
sudo adduser &lt;youruser&gt; libvirt
sudo adduser &lt;youruser&gt; libvirt-qemu
</pre>
What about checking <em>libvirtd</em> service status ?
<pre>sudo systemctl status libvirtd</pre>
And you might get errors like:
<pre>dnsmasqCapsRefreshInternal:741 : Cannot check dnsmasq binary /usr/sbin/dnsmasq: No such file or directory
virFirewallValidateBackend:193 : direct firewall backend requested, but /sbin/ebtables is not available: No such file or directory
virFirewallApply:916 : internal error: Failed to initialize a valid firewall backend</pre>
To fix those errors, we had to run:
<pre>sudo apt-get install ebtables dnsmasq
sudo systemctl restart libvirtd</pre>
<h2>Creating a new Guest</h2>
<h4>Using Virtual Manager</h4>
The Debian Wiki says "The easiest way to create and manage VM guest is using GUI application Virtual Machine Manager" but does not provide information on how to set it up.

I first tried to connect to the host machine from virt-manager installed on my laptop and got the following error
<pre>libvirtError: End of file while reading data: sh: nc: command not found</pre>
Pretty straightforward to fix you say? You have to install netcat on your host machine. But not any netcat, netcat-openbsd
<pre>sudo apt-get install netcat-openbsd</pre>
If you install netcat-traditional, it will raise the following error:
<pre>The remote host requires a version of netcat/nc which supports the -U option.</pre>

![a cat using a computer](../assets/imgs/netcat.jpg)

<h4>Using the Command Line</h4>
<pre>sudo virt-install --virt-type kvm --name yunoprod \
   --memory 8192 \
   --location http://cdn-fastly.deb.debian.org/debian/dists/jessie/main/installer-amd64/ \
   --extra-args "console=ttyS0" \
   -v \
   --disk path=/vm/yunoprod.img,format=raw,size=45 \
   --os-variant generic \
   --os-type linux \
   --vcpus 4</pre>
For some reason <em>--os-variant debianjessie</em> does not exist anymore.

And there we run in the following error:
<pre> ERROR Requested operation is not valid: network 'default' is not active</pre>
Which is fixed (until next reboot) by:
<pre> sudo virsh net-start default</pre>
To be continued...
