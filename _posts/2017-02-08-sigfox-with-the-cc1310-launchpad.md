Description
-----------

I try to use the CC1310F Launchpad by TI to communicate with the Sigfox infrastructure.

![CC1310LaunchPad]({{ "/assets/imgs/posts/CC1310LaunchPad.png" | absolute_url }})

Connecting with SmartRF
-----------------------

On Windows, run the SmartRF program. You should see two connected devices (one CC1310 LaunchPad has two interfaces).

Select the XDS110 Class Application/User UART.
At first, the device is "Unknown". You have to select the target device. I have a CC1310F128.

<img class="alignnone size-medium wp-image-332" src="https://kbl.netlib.re/wp-content/uploads/2017/02/Screenshot-from-2017-02-08-10-56-42-270x300.png" alt="" width="270" />

Right clic and "connect".
By default, the bootloader mode was not enabled so I got the following message:

<img class="alignnone size-full wp-image-337" src="https://kbl.netlib.re/wp-content/uploads/2017/02/Screenshot-from-2017-02-08-11-00-29.png" alt="" width="899" height="172" />

To enable the bootloader mode, hold the "SELECT" button (BTN-1, on the left) and press the "RESET" button (on the top).
Then try to connect again and you should see the following:

<img class="alignnone size-full wp-image-339" src="https://kbl.netlib.re/wp-content/uploads/2017/02/Screenshot-from-2017-02-08-11-05-49.png" alt="" width="900" height="169" />

You can now get board information such as the MAC address (we will need it soon).
<h2>Getting CCS project from Sigfox</h2>
The only way I found was to send an email to tech-pi-team@sigfox.com and ask for it. They replied with 3 hours.
<h2>Getting the Sigfox_data.h from TI</h2>
Send an email to sigfox@list.ti.com with the MAC address, your name and the company you work for.
They should reply with one sigfox_data.h per device.
<h2>Compile CCS project (GNU/Linux)</h2>
<ul>
 	<li>dowload and install CCS from <a href="http://processors.wiki.ti.com/index.php/Download_CCS">official TI Wiki</a> (I used 7.0.0)</li>
 	<li>dowload and install TI-RTOS <a href="http://www.ti.com/tool/ti-rtos-mcu">here</a>
<ul>
 	<li>I first had the following error with TI-RTOS version 2.21:
<code>Description Resource Path Location Type: identifier UARTCC26XX_HWAttrsV1 is undefined CC1310DK_7XD.c /TI_SIGFOX_CC13xx_v0.13 line 133 C/C++ Problem</code>
I then installed a previous version (2.18) following <a href="https://e2e.ti.com/support/wireless_connectivity/bluetooth_low_energy/f/538/t/547232">this post</a>.
It did not work.
I restarted CSS.
It worked.
<img class="alignnone size-full wp-image-353" src="https://kbl.netlib.re/wp-content/uploads/2017/02/no_idea.jpeg" alt="" width="281" height="179" /></li>
</ul>
I then found that I had to change <code>UARTCC26XX_HWAttrsV1</code> to <code>UARTCC26XX_HWAttrsV2</code> for it to work with the TI-RTOS version 2.21.</li>
</ul>
<h2>Flash the project onto the board</h2>
<ul>
 	<li>select the green bug to run the debugger (that will flash the board)</li>
 	<li>if you don't put the board in bootloading mode (as described in the first section), you will get errors:
<img class="alignnone wp-image-356" src="https://kbl.netlib.re/wp-content/uploads/2017/02/Screenshot-from-2017-02-08-14-33-54.png" alt="ccs_error1" width="300" />
<img class="alignnone wp-image-357" src="https://kbl.netlib.re/wp-content/uploads/2017/02/Screenshot-from-2017-02-08-14-34-09.png" alt="ccs_error2" width="300" />
<img class="alignnone wp-image-358" src="https://kbl.netlib.re/wp-content/uploads/2017/02/Screenshot-from-2017-02-08-14-34-29.png" alt="ccs_error3" width="300" /></li>
</ul>
Connect to the terminal (I use minicom) with the following parameters: <code>115200 8N1</code>
I got a nice: <code>Initialization error</code>
This is because I have to replace the file <code>sigfox_data.h</code> by the one that TI gave me.
<ul>
 	<li>replace the file apps/sigfox_data.h with the one provided by the TI support</li>
 	<li>select the green bug to run the debugger. It should be successful this time</li>
</ul>
<h2>Getting the Device ID and PAC number</h2>
Once the board is flashed, you can connect throught the terminal (minicom). No message should be displayed. To see what you are typing, enable "local echo".
<ul>
 	<li>Write the following and press enter: `AT$ID?``
A wild 8 digits device ID appears !
<img class="alignnone size-full wp-image-374" src="https://kbl.netlib.re/wp-content/uploads/2017/02/wild_deviceid.jpg" alt="" width="348" height="260" /></li>
 	<li>Write the following and press enter: `AT$PAC?``
You should get a 16 digits PAC number.</li>
</ul>
<h2>Activate your device</h2>
Activate you device using your device ID and PAC number on: <a href="https://backend.sigfox.com/">https://backend.sigfox.com/</a>
If it does not work, send an email to Sigfox again. I got a reply within a day. My device was activated after 3 days.
<h2>Send message</h2>
I had to change the MODE on the sigfox_demo.h file because I'm in Europe:
<code>//#define MODE_ARIB
#define MODE_ETSI</code>
<h2>Sigfox Backend Callbacks</h2>
On the sigfox backend, you can create callbacks to trigger some remote function or forward data.
Here is what I've done:

<img class="alignnone size-full wp-image-405" src="https://kbl.netlib.re/wp-content/uploads/2017/02/Screenshot-from-2017-04-07-15-45-20.png" alt="" width="731" height="645" />

I am sending the data in Json to a node-red instance on IBM Bluemix.
<h2>References</h2>
<ul>
 	<li><a href="http://processors.wiki.ti.com/index.php/Sigfox_CC1310_SDK_Demo_Users_Guide">CC1310 Sigfox user guide</a></li>
 	<li><a href="https://e2e.ti.com/group/launchyourdesign/m/simplelink/666680">TI tutorial</a></li>
 	<li><a href="http://sunmaysky.blogspot.fr/2016/08/how-to-use-backdoor-serial-boot-loader.html?m=1">Bootload</a></li>
</ul>
