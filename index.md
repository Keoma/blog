---
title: Bienvenue !
layout: default
---

Bienvenue !
===========

On this web site you will find the different projects I am working on, my last [publications](/publications/), the [things I like](/hobbies/) and my [resume](/resume/).

I am currently learning how to become a farmer 🧑‍🌾, and also building the [Cloud Girofle](https://girofle.cloud) and [Mémoriathèque](https://memoriatheque.fr) collectives.
I am looking for part-time computing/farming job offers. You can contact me at : contact(at)kbl.netlib.re.

I am the Co-Founder of Wattson Elements and its former CTO. I participated in the creation of [Falco](//wefalco.fr), a solution to help marina managers develop their customer services in a secure and reliable way.

I have a PhD on Wireless Sensor Networks, directed by [Thomas Watteyne](//twatteyne.wordpress.com/) at [Inria](//www.inria.fr) in the [EVA team](//team.inria.fr/eva/).

You can download my CV as PDF [in english here](/assets/pdfs/Brun-Laguna_Keoma_Resume.pdf) and [in french here (farming-oriented)](/assets/pdfs/Brun-Laguna_Keoma_CV-agri.pdf)

GPG: [CEF3CAA1](//pgp.mit.edu:11371/pks/lookup?op=get&amp;search=0xCEF3CAA1")
SSH: [public  key](/assets/id_rsa.pub)

